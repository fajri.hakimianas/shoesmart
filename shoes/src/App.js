import React from 'react'
import Header from './component/Header'
import './assets/style.css'
import Homepage from './pages/Homepage'
import { Route, Switch } from 'react-router'
import ProductList from './pages/ProductList'
import Cart from './pages/Cart'

const App = () => {
  return (
    <div className="app">
      <Switch>
        <Route exact path="/" component={Homepage}/>
        <Route exact path="/product-list" component={ProductList}/>
        <Route exact path="/cart" component={Cart}/>
      </Switch>
    </div>
  )
}

export default App
