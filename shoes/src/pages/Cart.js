import React from 'react'
import Header from '../component/Header'
import adidas from '../assets/image/adidas.png'

const Cart = () => {
    return (
        <>
            <Header data={"Product"}/>
            <div className="container-fluid mt-5">
                <div className="row">
                    <div className="col-md-8">
                        <div className="cart-info">
                            <div className="row">
                                <img src={adidas} alt="..."/>
                                <div className="col-6">
                                    <div className="card bg-light mt-2" style={{maxWidth: '27rem'}}>
                                        <div className="card-header">
                                            <button style={{width:'110px', marginRight:'40px', marginLeft:'-9px'}} type='button' className="btn btn-dark">Description</button>
                                            <button style={{width:'110px', marginRight:'30px', color:'black'}} type='button' className="btn btn-outline-light">Description</button>
                                            <button style={{width:'110px', color:'black'}} type='button' className="btn btn-outline-light">Description</button>
                                        </div>
                                        <div className="card-body">
                                            <p className="card-text">
                                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                                                when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="row">
                                <div className="img-grid">
                                    <div class="card bg-light" style={{maxWidth:'12rem', maxHeight:'170px'}}>
                                        <div class="card-body">
                                            <img src={adidas} alt=".."/>
                                        </div>
                                    </div>
                                    <div class="card bg-light" style={{maxWidth:'12rem', maxHeight:'170px'}}>
                                        <div class="card-body">
                                            <img src={adidas} alt=".."/>
                                        </div>
                                    </div>
                                    <div class="card bg-light" style={{maxWidth:'12rem', maxHeight:'170px'}}>
                                        <div class="card-body">
                                            <img src={adidas} alt=".."/>
                                        </div>
                                    </div>
                                    <div class="card bg-light" style={{maxWidth:'12rem', maxHeight:'170px'}}>
                                        <div class="card-body">
                                            <img src={adidas} alt=".."/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        checkout
                    </div>
                </div>
            </div>
        </>
    )
}

export default Cart
