import React from 'react'
import { Link } from 'react-router-dom'

const Sidebar = () => {
    return (
        <>
            <div className="sidebar">
                <div style={{marginLeft:'-4px'}}>
                    <ul className="nav flex-column">
                        <li className="nav-item">
                            <h5>Product</h5>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/product-list">
                                All
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/">
                                Men
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/">
                                Women
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/">
                                Kids
                            </Link>
                        </li>

                        <li className="nav-item mt-4">
                            <h5>Category</h5>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/">
                                All
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/">
                                Men
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/">
                                Women
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/">
                                Kids
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/">
                                All
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/">
                                Men
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/">
                                Women
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/">
                                Kids
                            </Link>
                        </li>

                        <li className="nav-item mt-4">
                            <h5>Information</h5>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/">
                                About Us
                            </Link>
                        </li>
                        <li className="nav-item" style={{marginTop:'-10px'}}>
                            <Link className="nav-link" to="/">
                                FAQ
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/">
                                Shipping
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        </>
    )
}

export default Sidebar
