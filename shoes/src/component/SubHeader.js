import React from 'react'
import { Link } from 'react-router-dom'

const SubHeader = () => {
    return (
        <>
            <div className="container-fluid mt-3">
                <div className="row">
                    <div className="col-1">
                        <Link to='/product-list' type="button" className="btn btn-dark">
                            <p>ALL BRANDS</p>
                        </Link>
                    </div>
                    <div className="col-1">
                        <Link type="button" className="btn btn-light">
                            <p>Avali</p>
                        </Link>
                    </div>
                    <div className="col-1">
                        <Link type="button" className="btn btn-light">
                            <p>Zetta</p>
                        </Link>
                    </div>
                    <div className="col-1">
                        <Link type="button" className="btn btn-light">
                            <p>Barefoot</p>
                        </Link>
                    </div>
                    <div className="col-1">
                        <Link type="button" className="btn btn-light">
                            <p>Perindu</p>
                        </Link>
                    </div>
                    <div className="col-1">
                        <Link type="button" className="btn btn-light">
                            <p>Shoelovin</p>
                        </Link>
                    </div>
                    <div className="col-1">
                        <Link type="button" className="btn btn-light">
                            <p>Misletoe</p>
                        </Link>
                    </div>
                    <div className="col-1">
                        <a className="btn" href="/" style={{width:'110px'}}>
                            <p>More Brands{" >"}</p>
                        </a>
                    </div>
                </div>
            </div>
        </>
    )
}

export default SubHeader
