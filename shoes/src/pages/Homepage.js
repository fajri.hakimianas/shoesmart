import React from 'react'
import ProductHero from './product/ProductHero'
import SubHeader from '../component/SubHeader'

import Sidebar from '../component/Sidebar'
import sepatu from '../assets/image/sepatu.png'
import women from '../assets/image/women.png'
import coming from '../assets/image/coming.png'
import ready from '../assets/image/ready.png'
import ProductNew from './product/ProductNew'
import Header from '../component/Header'

const Homepage = () => {
    return (
        <>
            <Header data={"Store"}/>
            <SubHeader/>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-1">
                        <Sidebar/>
                    </div>
                    <div className="col-11 mt-4">
                        <div className="row">
                            <div className="col-4" style={{marginLeft:'180px'}}>
                                <img src={sepatu} style={{width:'550px'}} alt="..."/>
                            </div>
                            <div className="col-4" style={{marginLeft:'130px'}}>
                                <img src={women} style={{width:'550px'}} alt="..."/>
                            </div>
                        </div>

                        {/* PRODUCT HERO */}
                        <ProductHero/>

                        {/* Banner */}
                        <div className="row">
                            <div className="col-12 mt-4">
                                <img src={coming} style={{height:'90%', width:'85%', marginLeft:'176px', borderRadius:'10px'}}/>
                            </div>
                        </div>

                        {/* PRODUCT NEW */}
                        <ProductNew/>
                    </div>
                </div>
            </div>
            <div className="container-fluid">
                <img src={ready} style={{width:'100%'}}/>
            </div>
        </>
    )
}

export default Homepage
