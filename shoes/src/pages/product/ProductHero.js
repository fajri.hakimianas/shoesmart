import React from 'react'
import adidas from '../../assets/image/adidas.png'

const ProductHero = () => {
    return (
        <div className="container mt-3">
            <div className="grid">
                <div className="card" style={{width: '14rem'}}>
                    <i className="bi bi-heart" style={{marginLeft:'90%'}}></i>
                    <img src={adidas} className="card-img-top mt-4" alt="..." style={{height:'45%'}}/>
                    <div className="card-body mt-4">
                        <p className="card-text">
                            Tony Perotty Boots
                            <br/>
                            <span>
                                <h6>Rp 600.000,-</h6>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProductHero