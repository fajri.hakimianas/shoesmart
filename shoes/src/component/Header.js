import React, { useState } from "react";
import { Link } from "react-router-dom";

const Header = ({data, styling}) => {

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <Link to='/' className="btn" style={{width:'150px'}}>
                <h5>{data}</h5>
            </Link>

            <div className="collapse navbar-collapse justify-content-center">
                <ul className="navbar-nav">
                    <li className="nav-item active">
                        <h2>Shoesmart</h2>
                    </li>
                </ul>
            </div>
            
            <button type='button' className="btn btn-outline mr-2">Login</button>
            <button type='button' className="btn btn-dark">Register</button>
        </nav>
    )
}

export default Header
