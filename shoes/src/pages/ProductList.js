import React from 'react'
import Header from '../component/Header'
import Sidebar from '../component/Sidebar'
import SubHeader from '../component/SubHeader'
import ProductAll from './product/ProductAll'

const ProductList = () => {
    return (
        <>
            <Header data={"Product List"}/>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-1">
                        <Sidebar/>
                    </div>
                    <div className="col-11 mt-3">
                        <div style={{marginLeft:'80px'}}>
                            <SubHeader/>
                        </div>

                        <div className="container-fluid">
                            <div className="box">
                                <div className='grid-list'>
                                    <div className="card-filter">
                                        {/* SORT */}
                                        <h6>Sort</h6>
                                        <div className="sort-grid">
                                            <button type="button" className="btn btn-outline">
                                                Ascending
                                            </button>
                                            <button type="button" className="btn btn-outline">
                                                Descending
                                            </button>
                                            <button type="button" className="btn btn-outline">
                                                Lower Price
                                            </button>
                                            <button type="button" className="btn btn-outline">
                                                Higher Price
                                            </button>
                                            <button type="button" className="btn btn-outline">
                                                Newest
                                            </button>
                                            <button type="button" className="btn btn-outline">
                                                Oldest
                                            </button>
                                        </div>
                                    </div>

                                    {/* COLOR */}
                                    <div className="card-filter">
                                        <h6>Color</h6>
                                        <div className="color-grid">
                                            <button type="button" className="btn btn-outline">
                                                Red
                                            </button>
                                            <button type="button" className="btn btn-outline">
                                                Yellow
                                            </button>
                                            <button type="button" className="btn btn-outline">
                                                Green
                                            </button>
                                            <button type="button" className="btn btn-outline">
                                                Black
                                            </button>
                                            <button type="button" className="btn btn-outline">
                                                Blue
                                            </button>
                                            <button type="button" className="btn btn-outline">
                                                Pink
                                            </button>
                                        </div>
                                    </div>
                                    
                                    {/* SIZE */}
                                    <div className="card-filter">
                                        <h6>Size</h6>
                                        <div className="size-grid">
                                            <button type="button" className="btn btn-outline">
                                                28
                                            </button>
                                            <button type="button" className="btn btn-outline">
                                                29
                                            </button>
                                            <button type="button" className="btn btn-outline">
                                                30
                                            </button>
                                            <button type="button" className="btn btn-outline">
                                                31
                                            </button>
                                            <button type="button" className="btn btn-outline">
                                                32
                                            </button>
                                            <button type="button" className="btn btn-outline">
                                                33
                                            </button>
                                            <button type="button" className="btn btn-outline">
                                                34
                                            </button>
                                            <button type="button" className="btn btn-outline">
                                                35
                                            </button>
                                            <button type="button" className="btn btn-outline">
                                                36
                                            </button>
                                            <button type="button" className="btn btn-outline">
                                                40
                                            </button>
                                        </div>
                                    </div>

                                    {/* PRICE RANGE */}
                                    <div className="card-filter">
                                        <h6>Price Range</h6>
                                        <div className="row">
                                            <div className="col-6">
                                                <p>Rp 0</p>
                                            </div>
                                            <div className="col-6">
                                                <p>Rp 200000</p>
                                            </div>
                                        </div>
                                        <div className="progress">
                                            <div className="progress-bar bg-danger" role="progressbar" style={{width:'50%'}} aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <ProductAll/>
                    </div>
                </div>
            </div>
        </>
    )
}

export default ProductList
